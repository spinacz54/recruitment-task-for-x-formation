package Task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    public static void main(String[] args) {

        int choice = 0;

        do {
            System.out.println("Please, select what would You like to order:\n1: Dish\n2: Drink\n3: Both");
            choice = getChoice(1,3);
        } while (choice == 0);


        Order order = new Order();

        switch(choice){
            case 1: order = orderDish(order); break;
            case 3: order = orderDish(order);                                   // 3 is for "both" option and has no break statement
            case 2: order = orderDrink(order); break;
        }

        order.printOrder();
    }

    public static int getChoice(int a, int b){
        String choice = "";

        try {
            choice = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int ch = 0;

        try {
            ch = Integer.parseInt(choice);
        } catch (NumberFormatException e) {
            System.out.println("Thats not a number!");
            ch = 0;
        }

        if((ch < a || ch > b) && ch != 0){
            System.out.println("Wrong number!");
            ch = 0;
        }

        return ch;
    }

    public static Order orderDish(Order order){
        
        CuisineMenu cuisineMenu = new CuisineMenu();
        Cuisine cuisine;

        int choice = 0;
        do {
            System.out.println("Please, select the cuisine:");
            System.out.println(cuisineMenu.getMenu());

            choice = getChoice(1,cuisineMenu.getNumberOfAvailableCuisines());
        } while (choice == 0);

        cuisine = cuisineMenu.getCuisine(choice);

        do {
            System.out.println("Please, select the dish:");
            System.out.println(cuisine.getMenu());

            choice = getChoice(1,cuisine.getNumberOfAvailableDishes());
        } while (choice == 0);

        order.setDish(cuisine.getDish(choice));

        return order;
    }

    public static Order orderDrink(Order order){

        DrinksMenu drinksMenu = new DrinksMenu();
        int choice = 0;

        do {
            System.out.println("Please, select the Drink");
            System.out.println(drinksMenu.getMenu());

            choice = getChoice(1,drinksMenu.getNumberOfAvailableDrinks());
        } while (choice == 0);

        Drink drink = drinksMenu.getDrink(choice);

        do {
            System.out.println("Would You like it with ice?\n1: Yes\n2: No");
            choice = getChoice(1,2);
        } while (choice == 0);

        if(choice == 1){
            drink.setWithIce(true);
        }else{
            drink.setWithIce(false);
        }


        do {
            System.out.println("Would You like it with lemon?\n1: Yes\n2: No");
            choice = getChoice(1,2);
        } while (choice == 0);

        if(choice == 1){
            drink.setWithLemon(true);
        }else{
            drink.setWithLemon(false);
        }

        order.setDrink(drink);

        return order;
    }
}
