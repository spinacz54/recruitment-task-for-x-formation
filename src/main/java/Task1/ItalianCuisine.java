package Task1;

public class ItalianCuisine extends Cuisine {

    public ItalianCuisine(int x) {
        this.name = "Italian Cuisine";
        this.dishesList.add(new Dish(1, "ItalianDish1", "Course1", "Dessert1", 10 ));
        this.dishesList.add(new Dish(2, "ItalianDish2", "Course2", "Dessert2", 20 ));
        this.dishesList.add(new Dish(3, "ItalianDish3", "Course3", "Dessert3", 30 ));
        this.id = x;
    }
}
