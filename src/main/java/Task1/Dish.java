package Task1;

public class Dish {

    int id;
    String name;
    String course;
    String dessert;
    int price;

    public Dish(int id, String name, String course, String dessert, int price) {
        this.id = id;
        this.name = name;
        this.course = course;
        this.dessert = dessert;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCourse() {
        return course;
    }

    public String getDessert() {
        return dessert;
    }

    public int getPrice() {
        return price;
    }
}
