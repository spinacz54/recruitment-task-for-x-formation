package Task1;

import java.util.ArrayList;
import java.util.List;

public class Cuisine {

    int id;
    String name;
    List<Dish> dishesList = new ArrayList();

    public String getMenu() {

        String menu = "";

        menu += "MENU:\n";
        for(Dish d : dishesList){
            menu += (d.getId() + ":   " + d.getName() + "   " + d.getPrice() + "\n");
        }
        return menu;
    }

    public int getNumberOfAvailableDishes(){
        return dishesList.size();
    }

    public Dish getDish(int no){
        if(no > 0 && no <= dishesList.size()){
            return dishesList.get(no - 1);
        }else{
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public void printMenu(){
        String res = "MENU:";

        for(Dish d: dishesList){
         res += (d.getId() + ": " + d.getName() + " ( " + d.getCourse() + " + " + d.getDessert() + " ) " + d.getPrice() + " USD\n");
        }
        System.out.println(res);
    }

    public int getId() {
        return id;
    }
}
