package Task1;

public class PolishCuisine extends Cuisine {

    public PolishCuisine(int x) {
        this.name = "Polish Cuisine";
        this.dishesList.add(new Dish(1, "PolishDish1", "Course1", "Dessert1", 10 ));
        this.dishesList.add(new Dish(2, "PolishDish2", "Course2", "Dessert2", 20 ));
        this.dishesList.add(new Dish(3, "PolishDish3", "Course3", "Dessert3", 30 ));
        this.id = x;
        
    }
}
