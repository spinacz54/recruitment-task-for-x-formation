package Task1;

public class MexicanCuisine extends Cuisine{

    public MexicanCuisine(int x) {
        this.name = "Mexican Cuisine";
        this.dishesList.add(new Dish(1, "MexicanDish1", "Course1", "Dessert1", 10 ));
        this.dishesList.add(new Dish(2, "MexicanDish2", "Course2", "Dessert2", 20 ));
        this.dishesList.add(new Dish(3, "MexicanDish3", "Course3", "Dessert3", 30 ));
        this.id = x;
    }
}
