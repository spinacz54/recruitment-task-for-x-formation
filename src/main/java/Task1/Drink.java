package Task1;

public class Drink {
    int id;
    String name;
    int price;
    boolean withIce;
    boolean withLemon;

    public Drink(int id, String name, int price) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.withIce = false;
        this.withLemon = false;
    }

    public Drink(int id, String name, int price, boolean withIce, boolean withLemon) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.withIce = withIce;
        this.withLemon = withLemon;
    }

    public int getPrice(){
        return price;
    }

    public String getName() {

        String res = name;

        if(withIce){
            res += " + ice";
        }

        if(withLemon){
            res += " + lemon";
        }

        return res;
    }

    public boolean isWithIce() {
        return withIce;
    }

    public boolean isWithLemon() {
        return withLemon;
    }

    public int getId() {
        return id;
    }

    public void setWithIce(boolean withIce) {
        this.withIce = withIce;
    }

    public void setWithLemon(boolean withLemon) {
        this.withLemon = withLemon;
    }
}
