package Task1;

public class Order {
    Dish dish;
    Drink drink;

    public Order() {
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public int getPrice(){
        int price = 0;
        if(dish != null){
            price += dish.getPrice();
        }
        if(drink != null){
            price += drink.getPrice();
        }
        return price;
    }

    public void printOrder(){
        String res = "Your order is:\n";
                if(dish != null){
                    res += (dish.getName() + " ( " + dish.getCourse() + " + " + dish.getDessert() + " ) " + dish.getPrice() + " USD\n");
                }
                if(drink != null){
                    res += (drink.getName() + " " + drink.getPrice() + " USD\n");
                }

                res += "Total price: " + getPrice() + " USD\n";

                System.out.println(res);
    }
}
