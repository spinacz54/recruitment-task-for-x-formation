package Task1;

import java.util.ArrayList;
import java.util.List;

public class DrinksMenu {

    List<Drink> drinksList;

    public DrinksMenu() {
        this.drinksList = new ArrayList<>();

        this.drinksList.add(new Drink(1, "DrinkName1", 4));
        this.drinksList.add(new Drink(2, "DrinkName2", 7));
        this.drinksList.add(new Drink(3, "DrinkName3", 5));
        this.drinksList.add(new Drink(4, "DrinkName4", 4));
        this.drinksList.add(new Drink(5, "DrinkName5", 10));
    }

    public String getMenu(){
        String res = "Menu:\n";

        for(Drink d : drinksList){
            res += d.getId() + ":   " + d.getName() + "   " + d.getPrice() + "\n";
        }

        return res;
    }

    public Drink getDrink(int no){
        if(no > 0 && no <= drinksList.size()){
            return drinksList.get(no - 1);
        }else{
            return null;
        }
    }

    public int getNumberOfAvailableDrinks(){
        return drinksList.size();
    }
}
