package Task1;

import java.util.ArrayList;
import java.util.List;

public class CuisineMenu {

    List<Cuisine> cuisineList;

    public CuisineMenu() {
        cuisineList = new ArrayList<>();
        cuisineList.add(new PolishCuisine(1));
        cuisineList.add(new MexicanCuisine(2));
        cuisineList.add(new ItalianCuisine(3));
    }

    public int getNumberOfAvailableCuisines(){
        return cuisineList.size();
    }

    public Cuisine getCuisine(int no){
        if(no > 0 && no <= cuisineList.size() ){
            return cuisineList.get(no - 1);
        }else{
            return null;
        }
    }

    public String getMenu(){
        String res = "Menu:\n";

        for(Cuisine c : cuisineList){
            res += c.getId() + ": " + c.getName() + "\n";
        }

        return res;
    }
}
