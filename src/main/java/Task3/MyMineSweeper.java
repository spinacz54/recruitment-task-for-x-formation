package Task3;

public class MyMineSweeper implements MineSweeper {

    public char[][] mineFieldArray;
    public int n;
    public int m;

    public void setMineField(String mineField) throws IllegalArgumentException {

        String parts[] = mineField.split("\n");

        int nChars = parts[0].length();

        for (int i = 1; i < parts.length; i++) {
            if (parts[i].length() != nChars) {
                throw new IllegalArgumentException();
            }
        }

        n = parts.length;
        m = parts[0].length();

        mineFieldArray = new char[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mineFieldArray[i][j] = parts[i].charAt(j);
            }
        }
    }

    public String getHintField() throws IllegalStateException {

        if(this.mineFieldArray == null){
            throw new IllegalStateException();
        }

        int mineCount;
        int k;
        int lStart;

        String res = "";

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                if( mineFieldArray[i][j] != '*') {

                    mineCount = 0;

                    k = 0;
                    lStart = 0;

                    if (i > 0) k = i - 1;
                    if (j > 0) lStart = j - 1;

                    for (; k <= i + 1; k++) {
                        for (int l = lStart; l <= j + 1; l++) {
                            if (k == i && l == j) continue;

                            if (k == n || l == m) continue;

                            if (mineFieldArray[k][l] == '*') mineCount++;
                        }
                    }

                    mineFieldArray[i][j] = (char) (mineCount + '0');
                }
                res += mineFieldArray[i][j];

            }
            res += "\n";
        }

        return res;
    }
}