package Task3;

public class Main {

    public static void main(String[] args) {

        String str = ".**.*\n*..*.\n....*\n***..\n...*.";
        //str = "*...\n..*.\n....";

        MineSweeper ms = new MyMineSweeper();

        System.out.println(str);
        System.out.println();

        ms.setMineField(str);

        System.out.println(ms.getHintField());

    }
}
